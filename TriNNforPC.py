# -------------------------------------------------------------------------
# TriNet implementation for Pulse Classification
# 
# Have three independently trained NNs for each main class and have the
# final classification be the joint output of the three.
# Use GMM results to train and test the model
# 
# -------------------------------------------------------------------------

from __future__ import print_function

import sys
import datetime
import random

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras import optimizers
from keras.optimizers import RMSprop
from keras.optimizers import Adam
from keras.optimizers import Adamax
from keras.optimizers import Nadam
from keras.optimizers import SGD
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint

from sklearn.model_selection import train_test_split

from math import *
import matplotlib
from matplotlib.colors import LogNorm
matplotlib.use('Agg')
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib import rc

font = {'family' : 'DejaVu Serif',
        'weight' : 'normal',
        'size'   : 12}

rc('font', **font)

import time

start_time = time.time()

#---------------------------------------------------------------------------------------------
#-------------------------- DATA AND OUTPUT LABELS, IO OPTIONS, ETC... -----------------------
#---------------------------------------------------------------------------------------------
# ------ Here we set the parameters for the neural net that will train over the weighed and
# ------ skimmed samples. The output names are also defined.
#---------------------------------------------------------------------------------------------
available_activations = ['relu ', 'sigmoid ', 'softmax ', 'softplus ', 'softsign ', 'tanh ', 'selu ','elu', 'exponential']
available_optimizers = ['SGD', 'RMSprop', 'Adam', 'Adadelta', 'Adagrad', 'Adamax', 'Nadam', 'Ftrl']

if len(sys.argv)!=7 :
    if len(sys.argv)==2 :
        if sys.argv[1] == 'man' :
            print('USAGE: python [script_name.py] - runs script with default settings')
            print('       python [script_name.py] [float]learning_rate [int]batch_size [int]layer_size [int]net_depth [str]activation_str [str]optimizer_str')
            sys.exit('Exiting.......')
        else :
            print('####### ERROR ####### Expected argv to be "man"')
            sys.exit('Exiting.......')
    elif len(sys.argv)==1 :
        learning_rate = 0.001
        batch_size = 128
        layer_size = 17
        net_depth = 3
        activation_str = 'elu'
        optimizer_str = 'RMSprop'
        optimizer = RMSprop(learning_rate)
        print('learning_rate    = {}'.format(learning_rate))
        print('batch_size       = {}'.format(batch_size))
        print('layer_size       = {}'.format(layer_size))
        print('net_depth        = {}'.format(net_depth))
        print('activation_str   = {}'.format(activation_str))
        print('optimizer_str    = {}'.format(optimizer_str))
    else :
        print('WARNING: Use "python [script_name.py] man" to chack usage')
        print('####### ERROR ####### Expected 3 argv, {} given'.format(len(sys.argv)-1))
        sys.exit('Exiting........')
else :
    try :
        learning_rate = float(sys.argv[1])
        batch_size = int(sys.argv[2])
        layer_size = int(sys.argv[3])
        net_depth = int(sys.argv[4])
        activation_str = str(sys.argv[5])
        optimizer_str = str(sys.argv[6])
        if learning_rate<0.00001 or learning_rate>10 :
            print('WARNING! learning rate low/high: setting to default of 0.001')
            learning_rate = 0.001
        elif batch_size<=0 or layer_size<=0 or net_depth<=0 :
            print('WARNING! batch_size, layer_size and net_depth must be positive: setting to default of 128, 17 and 3, respectively')
            batch_size = 128
            layer_size = 17
            net_depth = 3
        elif layer_size>500 or net_depth>20 :
            print('WARNING! layer_size and net_depth too large: setting to default of 17 and 3, respectively')
            print('This must be manually overridden in the source code....')
            batch_size = 128
            layer_size = 17
            net_depth = 3
        elif not(activation_str in str(available_activations)) :
            print('WARNING! specified activation is not available: setting to default of "elu"')
            activation_str = 'elu'
        else :
            print('learning_rate    = {}'.format(learning_rate))
            print('batch_size       = {}'.format(batch_size))
            print('layer_size       = {}'.format(layer_size))
            print('net_depth        = {}'.format(net_depth))
            print('activation_str   = {}'.format(activation_str))
        if optimizer_str in str(available_optimizers) :
            print('optimizer_str    = {}'.format(optimizer_str))
            if optimizer_str == 'SGD' :
                optimizer = SGD(learning_rate)
            elif optimizer_str == 'RMSprop' :
                optimizer = RMSprop(learning_rate)
            elif optimizer_str == 'Adam' :
                optimizer = Adam(learning_rate)
            elif optimizer_str == 'Adadelta' :
                optimizer = Adadelta(learning_rate)
            elif optimizer_str == 'Adagrad' :
                optimizer = Adagrad(learning_rate)
            elif optimizer_str == 'Adamax' :
                optimizer = Adamax(learning_rate)
            elif optimizer_str == 'Nadam' :
                optimizer = Nadam(learning_rate)
            elif optimizer_str == 'Ftrl' :
                optimizer = Ftrl(learning_rate)
            else :
                print('Something went terribly wrong....')
        else :
            print('WARNING! specified optimizer is not available: setting to default of "RMSprop"')
            optimizer_str = 'RMSprop'
            optimizer = RMSprop(learning_rate)
    except :
        print('####### ERROR ####### Wrong input format!! Should be: [int]n_trees [int OR None]max_depth [int]min_samples_split')
        sys.exit('Exiting.......')

test_fraction = 0.2
epochs = 500
loss = 'binary_crossentropy'
out_activation = 'sigmoid'

plotting = False;

filename = 'file.txt'
filename_gmm_results = 'GMM_fixedBase_67b.txt'
output_name = 'triNN_GMM_FIRtop6'

optimizer_str = '{}_{}'.format(optimizer_str,activation_str)
title_name = '{}\n{}'.format(output_name, optimizer_str)
date = datetime.datetime.today().strftime('%Y%m%d')

print('---------------------------------------------------------------------------------------------')
print('Importing data from: {}'.formet(filename))
print('---------------------------------------------------------------------------------------------')

data = np.genfromtxt(filename, delimiter='\t', dtype=None, encoding=None, names=('pulseClass', 'pA', 'pF50', 'TBA', 'pH', 'pL', 'pL90', 'aft5', 'aft25', 'aft50', 'aft75', 'aft95', 'pA100', 'pA200', 'pA500', 'pA1k', 'pA2k', 'pA5k', 'pHT', 'pRMSW', 'coincidence', 'rawFileN', 'rqFileN', 'rawEventID', 'rqEventID', 'pulseID'))

num_classes = 2 # One-vs-All
lenData = len(data)

print('---------------------------------------------------------------------------------------------')
print('Importing data from: {}'.format(filename_gmm_results))
print('---------------------------------------------------------------------------------------------')

gmm_results = np.loadtxt(filename_gmm_results, delimiter='\t')

gmm_bases = gmm_results[:,0]
if len(gmm_bases)!=lenData :
    print("ERROR!!! GMM data size not consistent with data size!! EXITING...")
    quit()

# mapping the GMM bases to observed classes (from handscanning)
gmm_classes = np.copy(gmm_bases)
gmm_classes[gmm_classes==0]=3
gmm_classes[gmm_classes==1]=4
gmm_classes[gmm_classes==2]=4
gmm_classes[gmm_classes==3]=2
gmm_classes[gmm_classes==4]=4
gmm_classes[gmm_classes==5]=4
gmm_classes[gmm_classes==6]=2
gmm_classes[gmm_classes==7]=3
gmm_classes[gmm_classes==8]=1
gmm_classes[gmm_classes==9]=4
gmm_classes[gmm_classes==10]=1
gmm_classes[gmm_classes==11]=1
gmm_classes[gmm_classes==12]=2
gmm_classes[gmm_classes==13]=4
gmm_classes[gmm_classes==14]=4
gmm_classes[gmm_classes==15]=4
gmm_classes[gmm_classes==16]=1
gmm_classes[gmm_classes==17]=4
gmm_classes[gmm_classes==18]=3
gmm_classes[gmm_classes==19]=2
gmm_classes[gmm_classes==20]=4
gmm_classes[gmm_classes==21]=4
gmm_classes[gmm_classes==22]=1
gmm_classes[gmm_classes==23]=2
gmm_classes[gmm_classes==24]=1
gmm_classes[gmm_classes==25]=1
gmm_classes[gmm_classes==26]=3
gmm_classes[gmm_classes==27]=2
gmm_classes[gmm_classes==28]=2
gmm_classes[gmm_classes==29]=2
gmm_classes[gmm_classes==30]=4
gmm_classes[gmm_classes==31]=2
gmm_classes[gmm_classes==32]=2
gmm_classes[gmm_classes==33]=2
gmm_classes[gmm_classes==34]=1
gmm_classes[gmm_classes==35]=4
gmm_classes[gmm_classes==36]=4
gmm_classes[gmm_classes==37]=4
gmm_classes[gmm_classes==38]=1
gmm_classes[gmm_classes==39]=4
gmm_classes[gmm_classes==40]=4
gmm_classes[gmm_classes==41]=2
gmm_classes[gmm_classes==42]=4
gmm_classes[gmm_classes==43]=3
gmm_classes[gmm_classes==44]=4
gmm_classes[gmm_classes==45]=1
gmm_classes[gmm_classes==46]=3
gmm_classes[gmm_classes==47]=4
gmm_classes[gmm_classes==48]=4
gmm_classes[gmm_classes==49]=2
gmm_classes[gmm_classes==50]=1
gmm_classes[gmm_classes==51]=3
gmm_classes[gmm_classes==52]=2
gmm_classes[gmm_classes==53]=4
gmm_classes[gmm_classes==54]=4
gmm_classes[gmm_classes==55]=2
gmm_classes[gmm_classes==56]=2
gmm_classes[gmm_classes==57]=2
gmm_classes[gmm_classes==58]=1
gmm_classes[gmm_classes==59]=2
gmm_classes[gmm_classes==60]=1
gmm_classes[gmm_classes==61]=4
gmm_classes[gmm_classes==62]=1
gmm_classes[gmm_classes==63]=1
gmm_classes[gmm_classes==64]=2
gmm_classes[gmm_classes==65]=4
gmm_classes[gmm_classes==66]=1

#------------------------------------------- Retain the original labels
pulseClass_s1 = np.array(gmm_classes)
pulseClass_s2 = np.array(gmm_classes)
pulseClass_se = np.array(gmm_classes)

#------------------------------------------- Making only two labels : 1 for S1 and 0 for the rest
pulseClass_s1[gmm_classes==2]=0
pulseClass_s1[gmm_classes==3]=0
pulseClass_s1[gmm_classes==4]=0
#------------------------------------------- Making only two labels : 1 for S2 and 0 for the rest
pulseClass_s2[gmm_classes==1]=0
pulseClass_s2[gmm_classes==2]=1
pulseClass_s2[gmm_classes==3]=0
pulseClass_s2[gmm_classes==4]=0
#------------------------------------------- Making only two labels : 1 for SE and 0 for the rest
pulseClass_se[gmm_classes==1]=0
pulseClass_se[gmm_classes==2]=0
pulseClass_se[gmm_classes==3]=1
pulseClass_se[gmm_classes==4]=0

#----------------------------------------- Creating a DataFrame of given dataset.
data=pd.DataFrame({
    'pA':data['pA'],
    'pF50':data['pF50'],
    'pF100':(data['pA100']/data['pA']),
    'pF200':(data['pA200']/data['pA']),
    'pF1k':(data['pA1k']/data['pA']),
    'tba':data['TBA'],
    'pL90':(data['aft95']-data['aft5']),
    'pH':data['pH'],
    'pHTL':(data['pHT']/data['pL']),
    'pRMSW':data['pRMSW'],
    'class':data['pulseClass']
})

pA = np.log10(np.array(data['pA']))
pF = np.array(data['pF100'])
pL = np.log10(np.array(data['pL90']))
tba = np.array(data['tba'])

#----------------------------------------- Extracting the classes of each pulse and changing Other=0 to Other=4
LZ_classes=np.array(data['class'])  # Labels

data = np.array([np.log10(data['pA']/(80.0)),
    data['pF100'],
    data['pF200'],
    data['tba'],
    np.log10(data['pL90']/(1000.0)),
    np.log10(data['pH'])])

num_params = len(data)
data = data.T

print('---------------------------------------------------------------------------------------------')
print('--------------------- DATA FORMATING COMPLETED - BUILDING MODEL FOR S1 ----------------------')
print('---------------------------------------------------------------------------------------------')

# Split dataset into training set and test set
x_train, x_test, y_train, y_test = train_test_split(data, pulseClass_s1, test_size=test_fraction, random_state=33) # 80% training and 20% test

#- convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

#-------------------------------------------------- build model_s1 architecture
model_s1 = Sequential() # constructor
model_s1.add(Dense(layer_size, activation='tanh', input_shape=(num_params,)))
model_s1.add(Dropout(0.1))
for nnn in range(net_depth):
    model_s1.add(Dense(layer_size, activation=activation_str))
    model_s1.add(Dropout(0.1))
model_s1.add(Dense(num_classes, activation=out_activation))

model_s1.summary()
model_s1.compile(loss = loss,
              optimizer = optimizer,
              metrics = ['accuracy'])

#-------------------------------------------------- Early stopping callback
monitor='val_loss'
patience=10
min_delta = 0.0
print('Early Stopping: monitoring {} for {} epochs with min delta {}'.format(monitor,patience,min_delta))
early_stopping_monitor = EarlyStopping(monitor=monitor, min_delta=min_delta, patience=patience, verbose=1, mode='auto')

#-------------------------------------------------- Model chackpoint callback
filepath = '/home/pbras/ML/NeuralNets/TriNetClassifier/PulseClassifier_s1_{}_{}_{}.hdf5'.format(output_name,optimizer_str,date)
checkpoint_model_s1 = ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

history_s1 = model_s1.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    callbacks=[early_stopping_monitor, checkpoint_model_s1],
                    validation_data=(x_test, y_test))

score = model_s1.evaluate(x_test, y_test, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

np_history_s1 = [history_s1.history["loss"], history_s1.history["acc"], history_s1.history["val_loss"], history_s1.history["val_acc"]]
np_history_s1 = np.array(np_history_s1)
np.savetxt('{}_{}_{}_training_data_s1.txt'.format(output_name,optimizer_str,date), np.transpose(np_history_s1))

print('-------------------------------------------------- History')
print(history_s1.history.keys())
plt.plot(history_s1.history['acc'])
plt.plot(history_s1.history['loss'])
plt.title('{} S1 NN - accuracy'.format(title_name))
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['acc', 'loss'], loc='upper left')
plt.savefig('IMAGES/{}_{}_{}_training_s1.png'.format(output_name,optimizer_str,date))
plt.clf()

plt.plot(history_s1.history['val_acc'])
plt.plot(history_s1.history['val_loss'])
plt.title('{} S1 NN - loss'.format(title_name))
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['acc', 'loss'], loc='upper left')
plt.savefig('IMAGES/{}_{}_{}_validation_s1.png'.format(output_name,optimizer_str,date))
plt.clf()


print('---------------------------------------------------------------------------------------------')
print('--------------------- DATA FORMATING COMPLETED - BUILDING MODEL FOR S2 ----------------------')
print('---------------------------------------------------------------------------------------------')

# Split dataset into training set and test set
x_train, x_test, y_train, y_test = train_test_split(data, pulseClass_s2, test_size=test_fraction, random_state=33) # 80% training and 20% test

#- convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

#-------------------------------------------------- build model_s2 architecture
model_s2 = Sequential() # constructor
model_s2.add(Dense(layer_size, activation='tanh', input_shape=(num_params,)))
model_s2.add(Dropout(0.1))
for nnn in range(net_depth):
    model_s2.add(Dense(layer_size, activation=activation_str))
    model_s2.add(Dropout(0.1))
model_s2.add(Dense(num_classes, activation=out_activation))

model_s2.summary()
model_s2.compile(loss = loss,
              optimizer = optimizer,
              metrics = ['accuracy'])

#-------------------------------------------------- Early stopping callback
monitor='val_loss'
patience=10
min_delta = 0.0
print('Early Stopping: monitoring {} for {} epochs with min delta {}'.format(monitor,patience,min_delta))
early_stopping_monitor = EarlyStopping(monitor=monitor, min_delta=min_delta, patience=patience, verbose=1, mode='auto')

#-------------------------------------------------- Model chackpoint callback
filepath = '/home/pbras/ML/NeuralNets/TriNetClassifier/PulseClassifier_s2_{}_{}_{}.hdf5'.format(output_name,optimizer_str,date)
checkpoint_model_s2 = ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

history_s2 = model_s2.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    callbacks=[early_stopping_monitor, checkpoint_model_s2],
                    validation_data=(x_test, y_test))

score = model_s2.evaluate(x_test, y_test, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

np_history_s2 = [history_s2.history["loss"], history_s2.history["acc"], history_s2.history["val_loss"], history_s2.history["val_acc"]]
np_history_s2 = np.array(np_history_s2)
np.savetxt('{}_{}_{}_training_data_s2.txt'.format(output_name,optimizer_str,date), np.transpose(np_history_s2))

print('-------------------------------------------------- History')
print(history_s2.history.keys())
plt.plot(history_s2.history['acc'])
plt.plot(history_s2.history['loss'])
plt.title('{} S2 NN - accuracy'.format(title_name))
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['acc', 'loss'], loc='upper left')
plt.savefig('IMAGES/{}_{}_{}_training_s2.png'.format(output_name,optimizer_str,date))
plt.clf()

plt.plot(history_s2.history['val_acc'])
plt.plot(history_s2.history['val_loss'])
plt.title('{} S2 NN - loss'.format(title_name))
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['acc', 'loss'], loc='upper left')
plt.savefig('IMAGES/{}_{}_{}_validation_s2.png'.format(output_name,optimizer_str,date))
plt.clf()


print('---------------------------------------------------------------------------------------------')
print('--------------------- DATA FORMATING COMPLETED - BUILDING MODEL FOR SE ----------------------')
print('---------------------------------------------------------------------------------------------')

# Split dataset into training set and test set
x_train, x_test, y_train, y_test = train_test_split(data, pulseClass_se, test_size=test_fraction, random_state=33) # 80% training and 20% test

#- convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

#-------------------------------------------------- build model_se architecture
model_se = Sequential() # constructor
model_se.add(Dense(layer_size, activation='tanh', input_shape=(num_params,)))
model_se.add(Dropout(0.1))
for nnn in range(net_depth):
    model_se.add(Dense(layer_size, activation=activation_str))
    model_se.add(Dropout(0.1))
model_se.add(Dense(num_classes, activation=out_activation))

model_se.summary()
model_se.compile(loss = loss,
              optimizer = optimizer,
              metrics = ['accuracy'])

#-------------------------------------------------- Early stopping callback
monitor='val_loss'
patience=10
min_delta = 0.0
print('Early Stopping: monitoring {} for {} epochs with min delta {}'.format(monitor,patience,min_delta))
early_stopping_monitor = EarlyStopping(monitor=monitor, min_delta=min_delta, patience=patience, verbose=1, mode='auto')

#-------------------------------------------------- Model chackpoint callback
filepath = '/home/pbras/ML/NeuralNets/TriNetClassifier/PulseClassifier_se_{}_{}_{}.hdf5'.format(output_name,optimizer_str,date)
checkpoint_model_se = ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

history_se = model_se.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    callbacks=[early_stopping_monitor, checkpoint_model_se],
                    validation_data=(x_test, y_test))

score = model_se.evaluate(x_test, y_test, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

np_history_se = [history_se.history["loss"], history_se.history["acc"], history_se.history["val_loss"], history_se.history["val_acc"]]
np_history_se = np.array(np_history_se)
np.savetxt('{}_{}_{}_training_data_se.txt'.format(output_name,optimizer_str,date), np.transpose(np_history_se))

print('-------------------------------------------------- History')
print(history_se.history.keys())
plt.plot(history_se.history['acc'])
plt.plot(history_se.history['loss'])
plt.title('{} SE NN - accuracy'.format(title_name))
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['acc', 'loss'], loc='upper left')
plt.savefig('IMAGES/{}_{}_{}_training_se.png'.format(output_name,optimizer_str,date))
plt.clf()

plt.plot(history_se.history['val_acc'])
plt.plot(history_se.history['val_loss'])
plt.title('{} SE NN - loss'.format(title_name))
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['acc', 'loss'], loc='upper left')
plt.savefig('IMAGES/{}_{}_{}_validation_se.png'.format(output_name,optimizer_str,date))
plt.clf()

print('---------------------------------------------------------------------------------------------')
print('------------------------------------- TRI NN COMPLETE  --------------------------------------')
print('---------------------------------------------------------------------------------------------')

