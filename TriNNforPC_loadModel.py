# -------------------------------------------------------------------------
# TriNet implementation for Pulse Classification
#
# Thsi script will load the TriNet model and plot the results
# 
# -------------------------------------------------------------------------

from __future__ import print_function

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras import optimizers
from keras.optimizers import RMSprop
from keras.optimizers import Adam
from keras.optimizers import Adamax
from keras.optimizers import Nadam
from keras.optimizers import SGD
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint

from sklearn.model_selection import train_test_split

from math import *
import sys
import datetime
import random
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import seaborn as sns
from matplotlib import rc

font = {'family' : 'DejaVu Serif',
        'weight' : 'normal',
        'size'   : 12}

rc('font', **font)

import time

start_time = time.time()

#---------------------------------------------------------------------------------------------
#-------------------------- DATA AND OUTPUT LABELS, IO OPTIONS, ETC... -----------------------
#---------------------------------------------------------------------------------------------
activation_str = 'elu'
optimizer_str = 'RMSprop'

test_fraction = 0.2
epochs = 500
loss = 'binary_crossentropy'
out_activation = 'sigmoid'

classifier_s1 = 'PulseClassifier_s1_triNN_GMM_FIRtop6_RMSprop_elu_20211208.hdf5'
classifier_s2 = 'PulseClassifier_s2_triNN_GMM_FIRtop6_RMSprop_elu_20211208.hdf5'
classifier_se = 'PulseClassifier_se_triNN_GMM_FIRtop6_RMSprop_elu_20211208.hdf5'
output_name = 'triNN_GMM_FIRtop6'

filename = 'file.txt'
filename_gmm_results = 'GMM_fixedBase_67b.txt'

optimizer_str = '{}_{}'.format(optimizer_str,activation_str)
date = datetime.datetime.today().strftime('%Y%m%d')

linspace_pA = np.linspace(-4.5,7.5,150)
linspace_pF = np.linspace(-5.0,1.0,150)
linspace_pL = np.linspace(1.0,6.0,150)
linspace_TBA = np.linspace(-1.2,1.2,150)

print('---------------------------------------------------------------------------------------------')
print('Importing data from: ',filename)
print('---------------------------------------------------------------------------------------------')

data = np.genfromtxt(filename, delimiter='\t', dtype=None, encoding=None, names=('pulseClass', 'pA', 'pF50', 'TBA', 'pH', 'pL', 'pL90', 'aft5', 'aft25', 'aft50', 'aft75', 'aft95', 'pA100', 'pA200', 'pA500', 'pA1k', 'pA2k', 'pA5k', 'pHT', 'pRMSW', 'coincidence', 'rawFileN', 'rqFileN', 'rawEventID', 'rqEventID', 'pulseID'))

coincidence = data['coincidence']

#----------------------------------------- Creating a DataFrame of given dataset.
data=pd.DataFrame({
    'pA':data['pA'],
    'pF50':data['pF50'],
    'pF100':(data['pA100']/data['pA']),
    'pF200':(data['pA200']/data['pA']),
    'pF1k':(data['pA1k']/data['pA']),
    'tba':data['TBA'],
    'pL90':(data['aft95']-data['aft5']),
    'pH':data['pH'],
    'class':data['pulseClass']
})

pulseClass=np.array(data['class'])  # Labels
pulseClass[pulseClass==0]=4

num_classes = 2
lenData = len(data)

print('---------------------------------------------------------------------------------------------')
print('Import data from: ',filename_gmm_results)
print('---------------------------------------------------------------------------------------------')

gmm_results = np.loadtxt(filename_gmm_results, delimiter='\t')

gmm_bases = gmm_results[:,0]
if len(gmm_bases)!=lenData :
    print("ERROR!!! GMM data size not consistent with data size!! EXITING...")
    quit()

# mapping the GMM bases to observed classes (from handscanning)
gmm_classes = np.copy(gmm_bases)
gmm_classes[gmm_classes==0]=3
gmm_classes[gmm_classes==1]=4
gmm_classes[gmm_classes==2]=4
gmm_classes[gmm_classes==3]=2
gmm_classes[gmm_classes==4]=4
gmm_classes[gmm_classes==5]=4
gmm_classes[gmm_classes==6]=2
gmm_classes[gmm_classes==7]=3
gmm_classes[gmm_classes==8]=1
gmm_classes[gmm_classes==9]=4
gmm_classes[gmm_classes==10]=1
gmm_classes[gmm_classes==11]=1
gmm_classes[gmm_classes==12]=2
gmm_classes[gmm_classes==13]=4
gmm_classes[gmm_classes==14]=4
gmm_classes[gmm_classes==15]=4
gmm_classes[gmm_classes==16]=1
gmm_classes[gmm_classes==17]=4
gmm_classes[gmm_classes==18]=3
gmm_classes[gmm_classes==19]=2
gmm_classes[gmm_classes==20]=4
gmm_classes[gmm_classes==21]=4
gmm_classes[gmm_classes==22]=1
gmm_classes[gmm_classes==23]=2
gmm_classes[gmm_classes==24]=1
gmm_classes[gmm_classes==25]=1
gmm_classes[gmm_classes==26]=3
gmm_classes[gmm_classes==27]=2
gmm_classes[gmm_classes==28]=2
gmm_classes[gmm_classes==29]=2
gmm_classes[gmm_classes==30]=4
gmm_classes[gmm_classes==31]=2
gmm_classes[gmm_classes==32]=2
gmm_classes[gmm_classes==33]=2
gmm_classes[gmm_classes==34]=1
gmm_classes[gmm_classes==35]=4
gmm_classes[gmm_classes==36]=4
gmm_classes[gmm_classes==37]=4
gmm_classes[gmm_classes==38]=1
gmm_classes[gmm_classes==39]=4
gmm_classes[gmm_classes==40]=4
gmm_classes[gmm_classes==41]=2
gmm_classes[gmm_classes==42]=4
gmm_classes[gmm_classes==43]=3
gmm_classes[gmm_classes==44]=4
gmm_classes[gmm_classes==45]=1
gmm_classes[gmm_classes==46]=3
gmm_classes[gmm_classes==47]=4
gmm_classes[gmm_classes==48]=4
gmm_classes[gmm_classes==49]=2
gmm_classes[gmm_classes==50]=1
gmm_classes[gmm_classes==51]=3
gmm_classes[gmm_classes==52]=2
gmm_classes[gmm_classes==53]=4
gmm_classes[gmm_classes==54]=4
gmm_classes[gmm_classes==55]=2
gmm_classes[gmm_classes==56]=2
gmm_classes[gmm_classes==57]=2
gmm_classes[gmm_classes==58]=1
gmm_classes[gmm_classes==59]=2
gmm_classes[gmm_classes==60]=1
gmm_classes[gmm_classes==61]=4
gmm_classes[gmm_classes==62]=1
gmm_classes[gmm_classes==63]=1
gmm_classes[gmm_classes==64]=2
gmm_classes[gmm_classes==65]=4
gmm_classes[gmm_classes==66]=1


print('---------------------------------------------------------------------------------------------')
print('---------- PARTITION THE DATA IN THE EXACT SAME WAY AS IN THE MODEL TRAINING MODULE ---------')
print('---------------------------------------------------------------------------------------------')

x_train, x_test, y_train, y_test, gmm_classes_train, gmm_classes_test, coincidence_train, coincidence = train_test_split(data, pulseClass, gmm_classes, coincidence, test_size=test_fraction, random_state=33) # 80% training and 20% test - Using the same random_state

pA = np.log10(np.array(x_test['pA'], copy=True))
pF = np.log10(np.array(x_test['pF50'], copy=True))
pL = np.log10(np.array(x_test['pL90'], copy=True))
tba = np.array(x_test['tba'], copy=True)

x_test = np.array([np.log10(x_test['pA']/(80.0)),
    x_test['pF100'],
    x_test['pF200'],
    x_test['tba'],
    np.log10(x_test['pL90']/(1000.0)),
    np.log10(x_test['pH'])])

num_params = len(x_test)
x_test = x_test.T

#------------------------------------------- Retain the original labels
y_test_s1 = np.array(gmm_classes_test, copy=True)
y_test_s2 = np.array(gmm_classes_test, copy=True)
y_test_se = np.array(gmm_classes_test, copy=True)

#------------------------------------------- Making only two labels : 1 for S1 and 0 for the rest
y_test_s1[y_test_s1==2]=0
y_test_s1[y_test_s1==3]=0
y_test_s1[y_test_s1==4]=0
#------------------------------------------- Making only two labels : 1 for S2 and 0 for the rest
y_test_s2[y_test_s2==1]=0
y_test_s2[y_test_s2==2]=1
y_test_s2[y_test_s2==3]=0
y_test_s2[y_test_s2==4]=0
#------------------------------------------- Making only two labels : 1 for SE and 0 for the rest
y_test_se[y_test_se==1]=0
y_test_se[y_test_se==2]=0
y_test_se[y_test_se==3]=1
y_test_se[y_test_se==4]=0


print('---------------------------------------------------------------------------------------------')
print('--------------------- DATA FORMATING COMPLETED - BUILDING MODEL FOR S1 ----------------------')
print('---------------------------------------------------------------------------------------------')

y_test_s1 = keras.utils.to_categorical(y_test_s1, num_classes)

#-------------------------------------------------- build model_s1 architecture
model_s1 = keras.models.load_model(classifier_s1)
model_s1.summary()

score = model_s1.evaluate(x_test, y_test_s1, verbose=1)
print('S1 Test loss:', score[0])
print('S1 Test accuracy:', score[1])

print('---------------------------------------------------------------------------------------------')
print('--------------------- DATA FORMATING COMPLETED - BUILDING MODEL FOR S2 ----------------------')
print('---------------------------------------------------------------------------------------------')

y_test_s2 = keras.utils.to_categorical(y_test_s2, num_classes)

#-------------------------------------------------- build model_s2 architecture
model_s2 = keras.models.load_model(classifier_s2)
model_s2.summary()

score = model_s2.evaluate(x_test, y_test_s2, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

print('---------------------------------------------------------------------------------------------')
print('--------------------- DATA FORMATING COMPLETED - BUILDING MODEL FOR SE ----------------------')
print('---------------------------------------------------------------------------------------------')

y_test_se = keras.utils.to_categorical(y_test_se, num_classes)

#-------------------------------------------------- build model_se architecture
model_se = keras.models.load_model(classifier_se)
model_se.summary()

score = model_se.evaluate(x_test, y_test_se, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])


print('---------------------------------------------------------------------------------------------')
print('--------------------- TRI NN COMPLETE - GOING TO CLASSIFY ALL THE DATA ----------------------')
print('---------------------------------------------------------------------------------------------')

results_s1 = model_s1.predict(x_test)
results_s2 = model_s2.predict(x_test)
results_se = model_se.predict(x_test)

np.savetxt('{}_results_s1.txt'.format(output_name),results_s1,delimiter='\t')
np.savetxt('{}_results_s2.txt'.format(output_name),results_s2,delimiter='\t')
np.savetxt('{}_results_se.txt'.format(output_name),results_se,delimiter='\t')

#confidence:
Gamma_ae = results_s1[:,0] + results_s2[:,0] + results_se[:,0]
Gamma_e = results_s1[:,1] + results_s2[:,1] + results_se[:,1]
p_s1 = np.zeros(len(Gamma_e))
p_s2 = np.zeros(len(Gamma_e))
p_se = np.zeros(len(Gamma_e))
p_oth = np.zeros(len(Gamma_e))
y_pred = np.zeros(len(Gamma_e))

# simplest probability 4-vector from the 3x2 results of the TriNet model
for i in range(len(Gamma_e)):
    if Gamma_e[i]<=1.0 :
        p_s1[i] = results_s1[i,1]
        p_s2[i] = results_s2[i,1]
        p_se[i] = results_se[i,1]
        p_oth[i] = 1-Gamma_e[i]
    else :
        p_oth[i] = 0.5*(Gamma_e[i]-1)
        p_s1[i] = results_s1[i,1]*(1-p_oth[i])/Gamma_e[i]
        p_s2[i] = results_s2[i,1]*(1-p_oth[i])/Gamma_e[i]
        p_se[i] = results_se[i,1]*(1-p_oth[i])/Gamma_e[i]


plt.subplot(221)
plt.hist(p_s1,bins=100)
plt.yscale("log")
plt.xlabel(r'p_${S1}$',fontsize=15)
plt.subplot(222)
plt.hist(p_s2,bins=100)
plt.yscale("log")
plt.xlabel(r'p_${S2}$',fontsize=15)
plt.subplot(223)
plt.hist(p_se,bins=100)
plt.yscale("log")
plt.xlabel(r'p_${SE}$',fontsize=15)
plt.subplot(224)
plt.hist(p_oth,bins=100)
plt.yscale("log")
plt.xlabel(r'p_${Oth}$',fontsize=15)
plt.savefig('{}_{}_probabilities.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_probabilities.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()


plt.hist2d(p_oth,p_s1,bins=50,cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'p_${Oth}$',fontsize=15)
plt.ylabel(r'p_${S1}$',fontsize=15)
plt.savefig('{}_{}_pOth_pS1.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_pOth_pS1.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()
plt.hist2d(p_oth,p_s2,bins=50,cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'p_${Oth}$',fontsize=15)
plt.ylabel(r'p_${S2}$',fontsize=15)
plt.savefig('{}_{}_pOth_pS2.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_pOth_pS2.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()
plt.hist2d(p_oth,p_se,bins=50,cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'p_${Oth}$',fontsize=15)
plt.ylabel(r'p_${SE}$',fontsize=15)
plt.savefig('{}_{}_pOth_pSE.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_pOth_pSE.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()
plt.hist2d(p_s1,p_s2,bins=50,cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'p_${S1}$',fontsize=15)
plt.ylabel(r'p_${S2}$',fontsize=15)
plt.savefig('{}_{}_pS1_pS2.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_pS1_pS2.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()
plt.hist2d(p_s1,p_se,bins=50,cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'p_${S1}$',fontsize=15)
plt.ylabel(r'p_${SE}$',fontsize=15)
plt.savefig('{}_{}_pS1_pSE.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_pS1_pSE.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()
plt.hist2d(p_s2,p_se,bins=50,cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'p_${S2}$',fontsize=15)
plt.ylabel(r'p_${SE}$',fontsize=15)
plt.savefig('{}_{}_pS2_pSE.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.savefig('{}_{}_pS2_pSE.pdf'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()


# Convert the 4-vector into categorical class labels using parametrized thresholds
s1_th = 0.51

for i in range(len(Gamma_e)):
    if p_s1[i]<0.01 :
        # S2-like and other
        if p_s2[i]<0.05 and p_se[i]<0.05 :
            y_pred[i]=4
        elif p_s2[i] > p_se[i] :
            y_pred[i]=2
        else :
            y_pred[i]=3
    elif p_s1[i]>s1_th :
        y_pred[i]=1
    else :
        y_pred[i]=4


#------------------------------------------- plots
for hh in [1, 2, 3, 4] :
    mask=y_pred==hh
    
    if hh==1 :
        cls_str = "S1"
    elif hh==2 :
        cls_str = "S2"
    elif hh==3 :
        cls_str = "SE"
    else :
        cls_str = "Other"

    plt.figure(figsize=(7.5, 6.25))
    plt.hist2d(pF[mask],pA[mask],bins=(linspace_pF,linspace_pA),cmap=plt.cm.viridis, norm=LogNorm())
    plt.xlabel('pF', fontsize=15)
    plt.ylabel(r'log$_{10}$(pA) [phd]', fontsize=15)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.text(-4.5,6.1,cls_str, fontsize=15,fontweight='bold')
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=14)
    plt.grid()
    plt.savefig('{}_{}_result_{}_hardLabel_pFpA.pdf'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_pFpA.eps'.format(output_name,optimizer_str,hh), pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_pFpA.png'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.cla()
    plt.clf()
    plt.figure(figsize=(7.5, 6.25))
    plt.hist2d(pA[mask],pL[mask],bins=(linspace_pA,linspace_pL),cmap=plt.cm.viridis, norm=LogNorm())
    plt.xlabel(r'log$_{10}$(pA) [phd]',fontsize=15)
    plt.ylabel(r'log$_{10}$(pL90) [ns]', fontsize=15)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.text(-3.2,5.5,cls_str, fontsize=15,fontweight='bold')
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=14)
    plt.grid()
    plt.savefig('{}_{}_result_{}_hardLabel_pApL.pdf'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_pApL.eps'.format(output_name,optimizer_str,hh), pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_pApL.png'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.cla()
    plt.clf()
    plt.figure(figsize=(7.5, 6.25))
    plt.hist2d(tba[mask],pA[mask],bins=(linspace_TBA,linspace_pA),cmap=plt.cm.viridis, norm=LogNorm())
    plt.xlabel('TBA', fontsize=15)
    plt.ylabel(r'log$_{10}$(pA) [phd]', fontsize=15)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.text(-1.0,6.1,cls_str, fontsize=15,fontweight='bold')
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=14)
    plt.grid()
    plt.savefig('{}_{}_result_{}_hardLabel_TBApA.pdf'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_TBApA.eps'.format(output_name,optimizer_str,hh), pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_TBApA.png'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.cla()
    plt.clf()
    plt.figure(figsize=(7.5, 6.25))
    plt.hist2d(tba[mask],pL[mask],bins=(linspace_TBA,linspace_pL),cmap=plt.cm.viridis, norm=LogNorm())
    plt.xlabel('TBA', fontsize=15)
    plt.ylabel(r'log$_{10}$(pL90) [ns]', fontsize=15)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.text(-1.0,5.5,cls_str, fontsize=15,fontweight='bold')
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=14)
    plt.grid()
    plt.savefig('{}_{}_result_{}_hardLabel_TBApL.pdf'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_TBApL.eps'.format(output_name,optimizer_str,hh), pad_inches = 0.2)
    plt.savefig('{}_{}_result_{}_hardLabel_TBApL.png'.format(output_name,optimizer_str,hh), bbox_inches = 'tight',pad_inches = 0.2)
    plt.cla()
    plt.clf()


#---------------------------------------------------------------
# Comparing with GMM
#---------------------------------------------------------------

print("---------------------------------- GMM Confusion matrix -------------------------------")
print("------y_pred ({} - {})".format(type(y_pred),len(y_pred)))
matConf = pd.crosstab(gmm_classes_test, y_pred, rownames=['GMM class'], colnames=['Predicted class'])
print(matConf)

matConf_np = matConf.values
acc_global = (matConf_np[0,0] + matConf_np[1,1] + matConf_np[2,2] + matConf_np[3,3])/float(np.sum(matConf_np))
acc_S1S2   = (matConf_np[0,0] + matConf_np[1,1] + matConf_np[1,2] + matConf_np[2,1] + matConf_np[2,2] + matConf_np[3,3])/float(np.sum(matConf_np))
print("Global accuracy (GMM) = {}".format(acc_global))
print("S1-S2 accuracy (GMM) = {}".format(acc_S1S2))

#------------------------------ Overal plots

plt.hist(Gamma_e,bins=50)
plt.yscale("log")
plt.ylabel('entries',fontsize=15)
plt.xlabel(r'Confidence, $\Gamma_{\epsilon}$',fontsize=15)
plt.savefig('{}_{}_Confidence.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

plt.hist(Gamma_ae,bins=50)
plt.yscale("log")
plt.ylabel('entries',fontsize=15)
plt.xlabel(r'Confidence, $\Gamma_{\tilde{\epsilon}}$',fontsize=15)
plt.savefig('{}_{}_Remainders.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

plt.hist2d(Gamma_e,Gamma_ae,bins=(np.linspace(0.0,2.0,50),np.linspace(1.0,3.0,50)),cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'Confidence, $\Gamma_{\epsilon}$', fontsize=15)
plt.ylabel(r'Complement, $\Gamma_{\tilde{\epsilon}}$', fontsize=15)
plt.savefig('{}_{}_overal_RespRem.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

#----------------------------- S1 plots

plt.hist2d(results_s1[:,1],results_s1[:,0],bins=(np.linspace(0.0,1.0,50),np.linspace(0.0,1.0,50)),cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'response, $\epsilon_{1}$',fontsize=15)
plt.ylabel(r'anti-response, $\tilde{\epsilon}_{1}$',fontsize=15)
plt.savefig('{}_{}_S1_response.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

#----------------------------- S2 plots

plt.hist2d(results_s2[:,1],results_s2[:,0],bins=(np.linspace(0.0,1.0,50),np.linspace(0.0,1.0,50)),cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'response, $\epsilon_{2}$',fontsize=15)
plt.ylabel(r'anti-response, $\tilde{\epsilon}_{2}$',fontsize=15)
plt.savefig('{}_{}_S2_response.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

#----------------------------- SE plots

plt.hist2d(results_se[:,1],results_se[:,0],bins=(np.linspace(0.0,1.0,50),np.linspace(0.0,1.0,50)),cmap=plt.cm.viridis,norm=LogNorm())
plt.colorbar()
plt.xlabel(r'response, $\epsilon_{3}$',fontsize=15)
plt.ylabel(r'anti-response, $\tilde{\epsilon}_{3}$',fontsize=15)
plt.savefig('{}_{}_SE_response.png'.format(output_name,optimizer_str), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()


#------------------------------------ translate the data to cartesian coords and make a ternary plot
x = 0.5 * np.divide(( 2.*results_s2[:,1]+results_se[:,1] ),(results_s1[:,1]+results_s2[:,1]+results_se[:,1]))
y = 0.5*np.sqrt(3) * np.divide(results_se[:,1],(results_s1[:,1]+results_s2[:,1]+results_se[:,1]))

plt.hist2d(x,y,bins=(np.linspace(-0.1,1.1,50),np.linspace(-0.1,0.97,50)), cmap=plt.cm.viridis, norm=LogNorm())
plt.colorbar()
plt.text(-0.05,-0.05,r'$\epsilon_1$',fontsize=15)
plt.text(1.02,-0.05,r'$\epsilon_2$',fontsize=15)
plt.text(0.5,0.9,r'$\epsilon_3$',fontsize=15)
plt.savefig('{}_ternary_response.png'.format(output_name), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

x = 0.5 * np.divide(( 2.*results_s2[:,0]+results_se[:,0] ),(results_s1[:,0]+results_s2[:,0]+results_se[:,0]))
y = 0.5*np.sqrt(3) * np.divide(results_se[:,0],(results_s1[:,0]+results_s2[:,0]+results_se[:,0]))

plt.hist2d(x,y,bins=(np.linspace(-0.1,1.1,50),np.linspace(-0.1,0.97,50)), cmap=plt.cm.viridis, norm=LogNorm())
plt.colorbar()
plt.text(-0.05,-0.05,r'$\tilde{\epsilon_1}$',fontsize=15)
plt.text(1.02,-0.05,r'$\tilde{\epsilon_2}$',fontsize=15)
plt.text(0.5,0.9,r'$\tilde{\epsilon_3}$',fontsize=15)
plt.savefig('{}_ternary_antiresponse.png'.format(output_name), bbox_inches = 'tight',pad_inches = 0.2)
plt.clf()

